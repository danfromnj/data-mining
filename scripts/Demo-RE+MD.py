#!/usr/bin/env python
# coding: utf-8

# In[10]:


import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from matplotlib import pyplot as plt


# In[11]:


data_file = "/mnt/d/wsl/dataset/connections.csv"
df_raw_data = pd.read_csv(data_file)
df_raw_data.groupby("attack").agg({'count':'count'}).reset_index("attack").sort_values(by='count', ascending=False)


# In[12]:


feature_names = ["srcbytes","dstbytes","land","wrongfragment","urgent","hot","numfailedlogins","loggedin","numcompromised","rootshell","suattempted","numroot","numfilecreations","numshells","numaccessfiles","numoutboundcmds","ishostlogin","isguestlogin","count","srvcount","serrorrate","srvserrorrate","rerrorrate","srvrerrorrate","samesrvrate","diffsrvrate","srvdiffhostrate","dsthostcount","dsthostsrvcount","dsthostsamesrvrate","dsthostdiffsrvrate","dsthostsamesrcportrate","dsthostsrvdiffhostrate","dsthostserrorrate","dsthostsrvserrorrate","dsthostrerrorrate","dsthostsrvrerrorrate"]
NORMAL = 'normal.'
THREAT = 'satan.'


# In[13]:


df_data = df_raw_data[(df_raw_data['attack']== NORMAL) | (df_raw_data['attack'] == THREAT)][feature_names]


# In[14]:


df_data


# In[15]:


scaler = StandardScaler(copy=True, with_mean=True, with_std=True).fit(df_data)
df_data_scaled = pd.DataFrame(data=scaler.transform(df_data),columns=feature_names)


# In[17]:


df_data_scaled


# In[19]:


pca = PCA(n_components=2).fit(df_data_scaled)
pca.components_


# In[23]:


type(pca.components_)


# In[21]:


# #original in scala
# def nre_func(pcm : Matrix) = udf[Double, Vector] { v => {
#  val pcmt = new breeze.linalg.DenseMatrix(pcm.numRows, pcm.numCols, pcm.toArray).t
#  val vt = DenseVector(v.toArray)
#  val projects = pcmt * vt
#  sqrt(vt.t * vt - projects.t * projects)
#  }
# }

def reconstruction_error(arr,pc,cols):
    l=[]
    for c in cols:
        l.append(arr[c])
    v = np.array(l)
    w = v*np.transpose(pc)
    return  np.sqrt(np.linalg.norm(v)**2 - np.linalg.norm(w)**2)

from datetime import datetime
print("started to compute reconstruction errors: " + str(datetime.now()))
df_data_scaled['re']=df_data_scaled.apply(reconstruction_error,axis=1,args=(np.asmatrix(pca.components_),feature_names))
print("finished computing reconstruction errors: " + str(datetime.now()))


# In[24]:


print("mean = %f" % df_data_scaled['re'].mean(axis=0))
print("median = %f" % df_data_scaled['re'].median(axis=0))


# In[25]:


df_data_threat = df_raw_data[df_raw_data['attack'] == THREAT][feature_names]
df_data_threat_scaled = pd.DataFrame(data=scaler.transform(df_data_threat),columns=feature_names)
df_data_threat_scaled['re']=df_data_threat_scaled.apply(reconstruction_error,axis=1,
                                                        args=(np.asmatrix(pca.components_),feature_names))


# In[26]:


print("mean = %f" %df_data_threat_scaled['re'].mean(axis=0))
print("median = %f" %df_data_threat_scaled['re'].median(axis=0))


# In[27]:


plt.hist(df_data_scaled['re'].values, bins =  np.arange(0,5,0.01)) 
plt.title("recontruction error for all data") 
plt.show()


# In[29]:


plt.hist(df_data_threat_scaled['re'].values, bins =  np.arange(0,5,0.01), color="orange") 
plt.title("recontruction error for attack data") 
plt.show()


# In[30]:


projections = pca.transform(df_data_scaled.drop(["re"], axis=1))
threat_projections = pca.transform(df_data_threat_scaled.drop(["re"], axis=1))
mahalanobis = np.array([np.linalg.norm(v) for v in projections])
threat_mahalanobis = np.array([np.linalg.norm(v) for v in threat_projections])


# In[7]:


# def mahalanobis_func(coeff : Matrix) = udf[Double, Vector] { v => {
#   val invCovariance = inv(new breeze.linalg.DenseMatrix(2, 2, coeff.toArray))
#   val vB = DenseVector(v.toArray)
#   sqrt(vB.t * invCovariance * vB)
#   }
# }


# In[31]:


plt.hist(mahalanobis, bins = np.arange(0,5,0.01)) 
plt.title("mahalanobis distance for all data") 
plt.show()


# In[32]:


plt.hist(threat_mahalanobis, bins = np.arange(0,5,0.01), color="r") 
plt.title("mahalanobis distance for  attack data") 
plt.show()

